from libqtile.extension.dmenu import Dmenu


class WindowList(Dmenu):
    defaults = [
        ("item_format", "{group}.{id}: {window}", "the format for the menu items"),
        ("all_groups", True, "If True, list windows from all groups; otherwise only from the current group"),
        ("dmenu_lines", "80", "Give lines vertically. Set to None get inline"),
    ]

    def __init__(self, **config):
        Dmenu.__init__(self, **config)
        self.add_defaults(WindowList.defaults)

    def _configure(self, qtile):
        Dmenu._configure(self, qtile)

    def list_windows(self):
        id = 0
        self.item_to_win = {}

        if self.all_groups:
            windows = self.qtile.windows_map.values()
        else:
            windows = self.qtile.current_group.windows

        windows = [(win, win.group.label or win.group.name) for win in windows if win.group]
        windows = sorted(windows, key=lambda x: x[1])

        for win, group in windows:
            if win.group:
                item = self.item_format.format(
                    group=group, id=id, window=win.name)
                self.item_to_win[item] = win
                id += 1

    def run(self):
        self.list_windows()
        out = super().run(items=self.item_to_win.keys())

        try:
            sout = out.rstrip('\n')
        except AttributeError:
            return

        try:
            win = self.item_to_win[sout]
        except KeyError:
            # The selected window got closed while the menu was open?
            return

        screen = self.qtile.current_screen
        screen.set_group(win.group)
        win.group.focus(win)
