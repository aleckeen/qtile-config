# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import subprocess
from typing import List  # noqa: F401

import libqtile
from libqtile import extension, layout
from libqtile.config import (Click, Drag, DropDown, Group, Key, ScratchPad,
                             Screen)
from libqtile.lazy import lazy

from bar import create_bar
from colors import colors
from extension.window_list import WindowList

term = "alacritty -e {command}"
tmux = "tmux -u"
terminal = term.format(command=tmux)
editor = "emacs"
browser = "firefox-nightly"
file_manager = term.format(command="ranger")
system_monitor = term.format(command="htop")
volume_control = "pavucontrol"

scartchpad = "scratchpad"
scratch_terminal = "terminal"
scratch_file_manager = "fm"
scratch_system_monitor = "sys"
scratch_volume = "volume"

dmenu_defaults = {
    "dmenu_font": "JetBrains Mono",
    "dmenu_ignorecase": True,
    "dmenu_prompt": ">",
    "background": colors[0],
    "foreground": colors[2],
    "selected_background": colors[5],
    "selected_foreground": colors[0],
}

dmenu_run = extension.dmenu.DmenuRun(**dmenu_defaults)
window_list = WindowList(
    **dmenu_defaults, item_format="{id} : {group} : {window}"
)

keys = [
    # Window manager
    Key(["mod4", "shift"], "j", lazy.shutdown()),
    Key(["mod4", "shift"], "k", lazy.restart()),

    # Navigation
    Key(["mod4"], "i", lazy.next_layout()),
    Key(["mod4", "shift"], "Tab", lazy.next_screen()),
    Key(["mod4"], "Tab", lazy.layout.next()),
    Key(["mod4", "shift"], "Tab", lazy.next_screen()),
    Key(["mod1"], "a", lazy.layout.left()),
    Key(["mod1"], "d", lazy.layout.right()),
    Key(["mod1"], "s", lazy.layout.down()),
    Key(["mod1"], "w", lazy.layout.up()),
    Key(["mod1", "shift"], "a", lazy.layout.swap_left()),
    Key(["mod1", "shift"], "d", lazy.layout.swap_right()),
    Key(["mod1", "shift"], "s", lazy.layout.shuffle_down()),
    Key(["mod1", "shift"], "w", lazy.layout.shuffle_up()),
    Key(["mod1", "control"], "a", lazy.layout.grow()),
    Key(["mod1", "control"], "d", lazy.layout.shrink()),
    Key(["mod1", "control"], "s", lazy.layout.normalize()),
    Key(["mod1", "control"], "w", lazy.layout.maximize()),

    # Window
    Key(["mod4", "shift"], "q", lazy.window.kill()),
    Key(["mod4", "shift"], "f", lazy.window.toggle_floating()),

    # Application
    Key(["mod4"], "Return", lazy.spawn(terminal)),
    Key(["mod4", "control"], "a", lazy.spawn(terminal)),
    Key(["mod4", "control"], "x", lazy.spawn(editor)),
    Key(["mod4", "control"], "z", lazy.spawn(browser)),
    Key(["mod4", "control"], "d", lazy.spawn(file_manager)),

    # Dropdowns
    Key(
        ["mod4", "shift"], "a",
        lazy.group[scartchpad].dropdown_toggle(scratch_terminal)
    ),
    Key(
        ["mod4", "shift"], "s",
        lazy.group[scartchpad].dropdown_toggle(scratch_volume)
    ),
    Key(
        ["mod4", "shift"], "d",
        lazy.group[scartchpad].dropdown_toggle(scratch_file_manager)
    ),
    Key(
        ["mod4", "shift"],
        "h",
        lazy.group[scartchpad].dropdown_toggle(scratch_system_monitor),
    ),

    # Run Menu
    Key(["mod4"], "r", lazy.run_extension(dmenu_run)),
    Key(["mod4", "shift"], "w", lazy.run_extension(window_list)),

    # Sound
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    Key(
        [], "XF86AudioLowerVolume",
        lazy.spawn("amixer sset Master 5%- unmute")
    ),
    Key(
        [], "XF86AudioRaiseVolume",
        lazy.spawn("amixer sset Master 5%+ unmute")
    ),
]

# Drag floating layouts.
mouse = [
    Drag(
        ["mod4"],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        ["mod4"],
        "Button3",
        lazy.window.set_size_floating(),
        start=lazy.window.get_size(),
    ),
    Click(["mod4"], "Button2", lazy.window.bring_to_front()),
]


workspaces = [
    (None, {"layout": "monadtall"}),  # 1 : main
    (None, {"layout": "monadtall"}),  # 2 : auxaliray main
    (None, {"layout": "monadtall"}),  # 3 : auxaliray main
    (None, {"layout": "monadtall"}),  # 4 : the rest
    (None, {"layout": "monadtall"}),  # 5 : the rest
    (None, {"layout": "monadtall"}),  # 6 : the rest
    (None, {"layout": "monadtall"}),  # 7 : the rest
    (None, {"layout": "monadtall"}),  # 8 : movies
    (None, {"layout": "max"}),        # 9 : comms
]

groups = []
for i, (ws, settings) in enumerate(workspaces, 1):
    group = Group(
        name=str(i) + (":" + ws if ws is not None else ""),
        **settings
    )
    groups.append(group)
    keys.extend(
        [
            Key(["mod4"], str(i), lazy.group[group.name].toscreen()),
            Key(["mod4", "shift"], str(i), lazy.window.togroup(group.name)),
        ]
    )

groups.append(
    ScratchPad(
        name=scartchpad,
        dropdowns=[
            DropDown(
                name=scratch_terminal,
                cmd=terminal,
                x=0.05,
                y=0.04,
                width=0.9,
                height=0.6,
                opacity=1,
            ),
            DropDown(
                name=scratch_file_manager,
                cmd=file_manager,
                x=0.05,
                y=0.05,
                width=0.9,
                height=0.9,
                opacity=1,
            ),
            DropDown(
                name=scratch_volume,
                cmd=volume_control,
                x=0.05,
                y=0.05,
                width=0.9,
                height=0.9,
                opacity=1,
            ),
            DropDown(
                name=scratch_system_monitor,
                cmd=system_monitor,
                x=0.05,
                y=0.05,
                width=0.9,
                height=0.9,
                opacity=1,
            ),
        ],
    )
)

theme = {
    "border_width": 2,
    "margin": 3,
    "border_focus": colors[6],
    "border_normal": colors[7],
}

widget_defaults = {
    "font": "JetBrains Mono",
    "fontsize": 12,
    "padding": 2,
    "background": colors[2],
}

extension_defaults = widget_defaults.copy()

layouts = [
    layout.MonadTall(**theme),
    layout.Max(**theme)
]

floating_layout = layout.Floating(
    **theme,
    float_rules=[
        {"wmclass": "confirm"},
        {"wmclass": "dialog"},
        {"wmclass": "download"},
        {"wmclass": "error"},
        {"wmclass": "file_progress"},
        {"wmclass": "notification"},
        {"wmclass": "splash"},
        {"wmclass": "toolbar"},
        {"wmclass": "confirmreset"},  # gitk
        {"wmclass": "makebranch"},  # gitk
        {"wmclass": "maketag"},  # gitk
        {"wname": "branchdialog"},  # gitk
        {"wname": "pinentry"},  # GPG key password entry
        {"wmclass": "Pinentry-gtk-2"},
        {"wmclass": "ssh-askpass"},  # ssh-askpass
        {"wmclass": "Sxiv"},
        {"wmclass": "vlc"},
        {"wmclass": "Clementine"},
        {"wmclass": "Rhythmbox"},
        {"wmclass": "Deadbeef"},
        {"wmclass": "Lollypop"},
        {"wmclass": "Xarchiver"},
    ]
)


@libqtile.hook.subscribe.screen_change
def restart_on_randr(qtile, ev):
    qtile.cmd_restart()


@libqtile.hook.subscribe.client_new
def client_new(window):
    if window.window.get_wm_class()[1] in ("discord", "Riot"):
        window.togroup(groups[8].label or groups[8].name)


monitors = int(
    subprocess.check_output(
        "xrandr --listactivemonitors | awk 'NR==1 {print $2}'", shell=True
    ).decode()
)


screens = [
    Screen(
        top=create_bar(i),
    )
    for i in range(monitors)
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = True
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
