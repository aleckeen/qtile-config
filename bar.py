from libqtile import bar, widget

from colors import colors


def create_seperator():
    return widget.Sep(
        linewidth=0,
        padding=6,
        foreground=colors[2],
        background=colors[0]
    )


def create_group_box():
    return widget.GroupBox(
        fontsize=9,
        margin_y=3,
        margin_x=0,
        padding_y=5,
        padding_x=2,
        borderwidth=3,
        active=colors[2],
        inactive=colors[2],
        rounded=False,
        highlight_color=colors[1],
        highlight_method="line",
        this_current_screen_border=colors[3],
        this_screen_border=colors[4],
        other_current_screen_border=colors[3],
        other_screen_border=colors[0],
        foreground=colors[2],
        background=colors[0],
    )


def create_prompt():
    return widget.Prompt(
        prompt="Run >>",
        padding=10,
        foreground=colors[3],
        background=colors[1],
    )


def create_window_name():
    return widget.WindowName(
        foreground=colors[6], background=colors[0], padding=0
    )


arrow_colors = (
    (colors[0], colors[5]),
    (colors[5], colors[4]),
    (colors[4], colors[5]),
    (colors[5], colors[0]),
    (colors[4], colors[0]),
)


def create_arrow(arrow_index):
    color = arrow_colors[arrow_index]
    return widget.TextBox(
        text="",
        background=color[0],
        foreground=color[1],
        padding=0,
        fontsize=37,
    )


def create_text_box(text, background):
    return widget.TextBox(
        text=text,
        foreground=colors[2],
        background=colors[5 if background == 0 else 4],
        padding=0,
        fontsize=14,
    )


def create_memory(background):
    return widget.Memory(
        foreground=colors[2],
        background=colors[5 if background == 0 else 4],
        format="Mem: {MemUsed}M",
        padding=5
    )


def create_cpu(background):
    return widget.CPU(
        format="CPU: {load_percent}%",
        foreground=colors[2],
        background=colors[5 if background == 0 else 4],
        padding=5,
    )


def create_net(background):
    return widget.Net(
        format="{down} ↓↑ {up}",
        foreground=colors[2],
        background=colors[5 if background == 0 else 4],
        padding=5,
    )


def create_volume(background):
    return widget.Volume(
        foreground=colors[2],
        background=colors[5 if background == 0 else 4],
        padding=5
    )


def create_current_layout(background):
    return widget.CurrentLayout(
        foreground=colors[2],
        background=colors[5 if background == 0 else 4],
        padding=5
    )


def create_clock(background):
    return widget.Clock(
        foreground=colors[2],
        background=colors[5 if background == 0 else 4],
        format="%A",
    )


def create_systray():
    return widget.Systray(background=colors[0], padding=5)


def create_bar(screen_index=0):
    widgets = [
        create_seperator(),
        create_group_box(),
        create_prompt(),
        create_seperator(),
        create_window_name(),

        create_arrow(0),
        create_memory(0),

        create_arrow(1),
        create_cpu(1),

        create_arrow(2),
        create_net(0),

        create_arrow(1),
        create_text_box("Vol:", 1),
        create_volume(1),

        create_arrow(2),
        create_current_layout(0),

        create_arrow(1),
        create_clock(1),

        create_arrow(4)
    ]

    if screen_index == 0:
        widgets.append(create_systray())
    widgets.append(create_seperator())

    return bar.Bar(widgets, 16)
